package com.example.spba.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class SensorData{
    @TableId(type = IdType.AUTO)

    @JsonProperty("DID")
    private Long DID;

    @JsonProperty("JID")
    private Integer JID;

    @JsonProperty("Pressure")
    private Double Pressure;

    @JsonProperty("Level")
    private Double Level;

    @JsonProperty("WG1")
    private boolean WG1;

    @JsonProperty("WG2")
    private boolean WG2;

    @JsonProperty("FE1")
    private boolean FE1;

    @JsonProperty("FE2")
    private boolean FE2;

    @JsonProperty("FE3")
    private boolean FE3;

    @JsonProperty("D1")
    private boolean D1;

    @JsonProperty("D2")
    private boolean D2;

    @JsonProperty("D3")
    private boolean D3;

    @JsonProperty("HS")
    private boolean HS;

    @JsonProperty("H1")
    private boolean H1;

    @JsonProperty("H2")
    private boolean H2;

    @JsonProperty("Time")
    private Date Time;
    private Long id;

    public Long getDID() {
        return DID;
    }

    public void setDID(Long DID) {
        this.DID = DID;
    }

    public Integer getJID() {
        return JID;
    }

    public void setJID(Integer JID) {
        this.JID = JID;
    }

    public Double getPressure() {
        return Pressure;
    }

    public void setPressure(Double pressure) {
        Pressure = pressure;
    }

    public Double getLevel() {
        return Level;
    }

    public void setLevel(Double level) {
        Level = level;
    }

    public boolean isWG1() {
        return WG1;
    }

    public void setWG1(boolean WG1) {
        this.WG1 = WG1;
    }

    public boolean isWG2() {
        return WG2;
    }

    public void setWG2(boolean WG2) {
        this.WG2 = WG2;
    }

    public boolean isFE1() {
        return FE1;
    }

    public void setFE1(boolean FE1) {
        this.FE1 = FE1;
    }

    public boolean isFE2() {
        return FE2;
    }

    public void setFE2(boolean FE2) {
        this.FE2 = FE2;
    }

    public boolean isFE3() {
        return FE3;
    }

    public void setFE3(boolean FE3) {
        this.FE3 = FE3;
    }

    public boolean isD1() {
        return D1;
    }

    public void setD1(boolean d1) {
        D1 = d1;
    }

    public boolean isD2() {
        return D2;
    }

    public void setD2(boolean d2) {
        D2 = d2;
    }

    public boolean isD3() {
        return D3;
    }

    public void setD3(boolean d3) {
        D3 = d3;
    }

    public boolean isHS() {
        return HS;
    }

    public void setHS(boolean HS) {
        this.HS = HS;
    }

    public boolean isH1() {
        return H1;
    }

    public void setH1(boolean h1) {
        H1 = h1;
    }

    public boolean isH2() {
        return H2;
    }

    public void setH2(boolean h2) {
        H2 = h2;
    }

    public Date getTime() {
        return Time;
    }

    public void setTime(Date time) {
        Time = time;
    }

    public SensorData() {
    }

    public SensorData(Long DID, Integer JID, Double pressure, Double level, boolean WG1, boolean WG2, boolean FE1, boolean FE2, boolean FE3, boolean d1, boolean d2, boolean d3, boolean HS, boolean h1, boolean h2, Date time) {
        this.DID = DID;
        this.JID = JID;
        Pressure = pressure;
        Level = level;
        this.WG1 = WG1;
        this.WG2 = WG2;
        this.FE1 = FE1;
        this.FE2 = FE2;
        this.FE3 = FE3;
        D1 = d1;
        D2 = d2;
        D3 = d3;
        this.HS = HS;
        H1 = h1;
        H2 = h2;
        Time = time;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
