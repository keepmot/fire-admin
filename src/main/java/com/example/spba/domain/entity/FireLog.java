package com.example.spba.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by macro on 2023/12/21
 */
@Data
public class FireLog implements Serializable {

    private static final long serialVersionUID = 470786259004016587L;

    /** Alarm_Id */
    @TableId(type = IdType.AUTO)
    private Long id;


    /**Alarm_Time*/
    @TableField(fill = FieldFill.INSERT)
    private Date time;



}
