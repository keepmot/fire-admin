package com.example.spba.domain.entity;

import java.io.Serializable;

/**
 * Created by macro on 2023/11/26
 */
public class User implements Serializable{

    private Long id;

    private String name;

    private String password;


    public User(Long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
