package com.example.spba.domain.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by macro on 2024/2/25
 */
@Data
public class Equipment implements Serializable {
    private Long id;
    private String deviceId;
    private String deviceName;
    private Integer deviceStatus;
    private Date time;

}
