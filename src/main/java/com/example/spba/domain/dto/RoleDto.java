package com.example.spba.domain.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoleDto
{

    @NotNull(message = "参数错误", groups = RoleDto.Update.class)
    @Min(value = 1, message = "参数错误", groups = RoleDto.Update.class)
    private Long id;

    @NotBlank(message = "请输入角色名称", groups = {RoleDto.Save.class, RoleDto.Update.class})
    private String name;

    @NotBlank(message = "请选择角色权限", groups = {RoleDto.Save.class, RoleDto.Update.class})
    private String menuIds;

    private Integer status;

    /**
     * 保存时的校验分组
     */
    public interface Save{}

    /**
     * 更新时的校验分组
     */
    public interface Update{}
}
