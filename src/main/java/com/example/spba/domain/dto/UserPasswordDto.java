package com.example.spba.domain.dto;

import lombok.Data;

/**
 * Created by macro on 2023/11/26
 */

@Data
public class UserPasswordDto {

    private String username;

    private String phone;

    private String password;

    private String newPassword;

    private String confirmPassword;






}
