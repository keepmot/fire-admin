package com.example.spba.exception;

/**
 * Created by macro on 2023/11/27
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }
}

