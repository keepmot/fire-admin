package com.example.spba.exception;

/**
 * Created by macro on 2023/11/27
 */
public class ServiceException extends Exception {

    // 现有的构造函数，可保留
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    // 修改后的构造函数，仅接受错误消息
    public ServiceException(String message) {
        super(message);
    }
}

