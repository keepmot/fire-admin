package com.example.spba.amqp;



import com.example.spba.dao.SensorDataMapper;
import com.example.spba.domain.entity.SensorData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class CustomMessageListener implements MessageListener {

    private final Logger logger = LoggerFactory.getLogger(CustomMessageListener.class);


    @Autowired
    private SensorDataMapper sensorDataMapper;


    private final ObjectMapper objectMapper = new ObjectMapper(); // JSON解析器

    @Override
    public void onMessage(Message message) {


        try {
            byte[] body = message.getBody(byte[].class);
            String content = new String(body);
           // logger.info(content);
            JsonNode jsonNode = objectMapper.readTree(content);
            try {
                jsonNode = objectMapper.readTree(content);
            } catch (Exception e) {
                logger.error("Invalid JSON format: " + content, e);
                return; //
            }

            SensorData sensorData1 = new SensorData();
            sensorData1.setD1(jsonNode.path("D1").asBoolean());
            sensorData1.setD2(jsonNode.path("D2").asBoolean());
            sensorData1.setD3(jsonNode.path("D3").asBoolean());
            sensorData1.setHS(jsonNode.path("HS").asBoolean());
            sensorData1.setH1(jsonNode.path("H1").asBoolean());
            sensorData1.setH2(jsonNode.path("H2").asBoolean());
            sensorData1.setWG1(jsonNode.path("WG1").asBoolean());
            sensorData1.setWG2(jsonNode.path("WG2").asBoolean());
            sensorData1.setFE1(jsonNode.path("FE1").asBoolean());
            sensorData1.setFE2(jsonNode.path("FE2").asBoolean());
            sensorData1.setFE3(jsonNode.path("FE3").asBoolean());
            sensorData1.setDID(jsonNode.path("DID").asLong());
            sensorData1.setJID(jsonNode.path("JID").asInt());
            sensorData1.setPressure(jsonNode.path("Pressure").asDouble());
            sensorData1.setLevel(jsonNode.path("Level").asDouble());
            String lastTimeString = jsonNode.get("Time").asText();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date parsedDate = dateFormat.parse(lastTimeString);
                Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            sensorData1.setTime(timestamp);

            int insert=sensorDataMapper.insert(sensorData1);
                if(insert == 1) {
                    logger.info("Message inserted into database successfully.");
                } else {
                    logger.error("Failed to insert message into database.");
                }

        } catch (Exception e) {
            logger.error("An error occurred while processing message", e);
        }
    }
}
