package com.example.spba.amqp;


import org.apache.commons.codec.binary.Base64;
import org.apache.qpid.jms.JmsConnection;
import org.apache.qpid.jms.JmsConnectionListener;
import org.apache.qpid.jms.message.JmsInboundMessageDispatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.net.URI;
import java.util.Hashtable;

@Component
public class AmqpClient {

    @Autowired
    private CustomMessageListener customMessageListener;


    private final static Logger logger = LoggerFactory.getLogger(AmqpClient.class);

    @PostConstruct
    public void start() {
        try {
            // 参数设置
            String accessKey = "LTAI5tGBtcXd66BzKLEwfD8d";
            String accessSecret = "glRek7VQqPsQrBIQRX7KKxx79RDVDC";
            String consumerGroupId = "DjtKJWeNgp2GzKIE2dUm000100";
            String iotInstanceId = "iot-06z00d8mzc5mt0z";
            long timeStamp = System.currentTimeMillis();
            String signMethod = "hmacsha1";
            String clientId = "123";

            // 计算签名
            String userName = clientId + "|authMode=aksign"
                    + ",signMethod=" + signMethod
                    + ",timestamp=" + timeStamp
                    + ",authId=" + accessKey
                    + ",iotInstanceId=" + iotInstanceId
                    + ",consumerGroupId=" + consumerGroupId
                    + "|";
            String signContent = "authId=" + accessKey + "&timestamp=" + timeStamp;
            String password = doSign(signContent, accessSecret, signMethod);

            // 设置连接URL
            String connectionUrl = "failover:(amqps://iot-06z00d8mzc5mt0z.amqp.iothub.aliyuncs.com:5671?amqp.idleTimeout=80000)"
                    + "?failover.reconnectDelay=30";

            // 设置连接工厂
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("connectionfactory.SBCF", connectionUrl);
            hashtable.put("queue.QUEUE", "default");
            hashtable.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.qpid.jms.jndi.JmsInitialContextFactory");
            Context context = new InitialContext(hashtable);
            ConnectionFactory cf = (ConnectionFactory) context.lookup("SBCF");
            Destination queue = (Destination) context.lookup("QUEUE");

            // 创建连接
            Connection connection = cf.createConnection(userName, password);
            ((JmsConnection) connection).addConnectionListener(myJmsConnectionListener);
            // 创建会话
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
            // 创建接收器连接，并设置自定义的消息监听器
            MessageConsumer consumer = session.createConsumer(queue);
            consumer.setMessageListener(customMessageListener);
        } catch (Exception e) {
            logger.error("An error occurred while starting AMQP client", e);
        }
    }


    private  JmsConnectionListener myJmsConnectionListener = new JmsConnectionListener() {
        @Override
        public void onConnectionEstablished(URI remoteURI) {
            logger.info("Connection established to {}", remoteURI);
        }

        @Override
        public void onConnectionFailure(Throwable error) {
            logger.error("Connection failure: {}", error.getMessage());
        }

        @Override
        public void onConnectionInterrupted(URI remoteURI) {
            logger.info("Connection interrupted to {}", remoteURI);
        }

        @Override
        public void onConnectionRestored(URI remoteURI) {
            logger.info("Connection restored to {}", remoteURI);
        }

        @Override
        public void onInboundMessage(JmsInboundMessageDispatch envelope) {
        }

        @Override
        public void onSessionClosed(Session session, Throwable cause) {
        }

        @Override
        public void onConsumerClosed(MessageConsumer consumer, Throwable cause) {
        }

        @Override
        public void onProducerClosed(MessageProducer producer, Throwable cause) {
        }
    };

    private static String doSign(String toSignString, String secret, String signMethod) throws Exception {
        SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), signMethod);
        Mac mac = Mac.getInstance(signMethod);
        mac.init(signingKey);
        byte[] rawHmac = mac.doFinal(toSignString.getBytes());
        return Base64.encodeBase64String(rawHmac);
    }

}

