package com.example.spba.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GetUserByUsernameMapper {

    @Select("select * from user where username = #{username}")
    SecurityProperties.User getUserByUsername (@Param("username") String username);

}
