package com.example.spba.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spba.domain.dto.UserPasswordDto;
import com.example.spba.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Update("update admin set password = #{newPassword} where username = #{username} and password = #{password}")
    int updatePassword(UserPasswordDto userPasswordDto);
}
