package com.example.spba;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.example.spba.repository")
@SpringBootApplication
public class SpbaApiApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SpbaApiApplication.class, args);
    }

}