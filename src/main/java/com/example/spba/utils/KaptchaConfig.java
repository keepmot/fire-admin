package com.example.spba.utils;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class KaptchaConfig {

    @Bean
    public DefaultKaptcha captchaProducer() {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        // 配置 kaptcha，例如添加字体、文本长度等
        Config config = new Config(new Properties());
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
