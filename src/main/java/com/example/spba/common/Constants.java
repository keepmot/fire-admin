package com.example.spba.common;

/**
 * Created by macro on 2023/11/27
 */
public interface Constants {
    String CODE_200="200"; //成功
    String CODE_500="500"; //系统错误
    String CODE_400="400"; //系统错误
    String CODE_401="401"; //系统错误
    String CODE_600="600"; //系统错误

}
