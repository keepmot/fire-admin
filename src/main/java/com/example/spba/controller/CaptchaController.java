package com.example.spba.controller;

import com.example.spba.service.CaptchaService;
import com.example.spba.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.code.kaptcha.impl.DefaultKaptcha;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Base64;

@RestController
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private DefaultKaptcha defaultKaptcha;

    @GetMapping("/captcha")
    public R captcha(HttpServletRequest request, HttpServletResponse response) {
        try {
            // 设置响应内容类型
            response.setContentType("text/html;charset=UTF-8");

            // 生成验证码字符串
            String captcha = captchaService.generateCaptcha();

            // 将验证码字符串存入Session
            HttpSession session = request.getSession();
            session.setAttribute("captcha", captcha);

            // 生成验证码图片
            BufferedImage image = defaultKaptcha.createImage(captcha);

            // 将验证码图片转换为Base64编码字符串
            String captchaBase64 = imageToBase64(image);

            return R.success(captchaBase64);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("生成验证码失败");
        }
    }

    // 将图片转换为Base64编码字符串
    private String imageToBase64(BufferedImage image) throws IOException {
        String format = "png"; // 图片格式
        String base64Prefix = "data:image/" + format + ";base64,"; // Base64编码前缀
        // 将图片转换为Base64编码字符串
        String base64String;
        try (java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream()) {
            ImageIO.write(image, format, bos);
            byte[] imageBytes = bos.toByteArray();
            base64String = base64Prefix + Base64.getEncoder().encodeToString(imageBytes);
        }
        return base64String;
    }
}


