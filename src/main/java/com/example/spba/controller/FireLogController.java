package com.example.spba.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.spba.service.EquipmentService;
import com.example.spba.service.FireLogService;
import com.example.spba.service.FiredataService;
import com.example.spba.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by macro on 2023/12/20
 */
@RestController
public class FireLogController {

    @Autowired
    private FireLogService fireLogService;

    @Autowired
    private FiredataService firedataService;

    @Autowired
    private EquipmentService equipmentService;

    /**
     */
    @GetMapping("/fire/log")
    public R getFireLogList(String nodeId, String start, String end,
                             @RequestParam(name = "page", defaultValue = "1") Integer page,
                             @RequestParam(name = "size", defaultValue = "15") Integer size)
    {
        HashMap where = new HashMap();
        where.put("nodeId",nodeId);
        where.put("start", start);
        where.put("end", end);

        Page<HashMap> pages = new Page<>(page, size);
        Page<HashMap> list = fireLogService.getList(pages, where);


        return R.success(list);

    }

    @GetMapping("/fire/data")
    public R getFiredataList(String nodeId, String start, String end,
                            @RequestParam(name = "page", defaultValue = "1") Integer page,
                            @RequestParam(name = "size", defaultValue = "15") Integer size)
    {
        HashMap where = new HashMap();
        where.put("nodeId",nodeId);
        where.put("start", start);
        where.put("end", end);

        Page<HashMap> pages = new Page<>(page, size);
        Page<HashMap> list = firedataService.getList(pages, where);


        return R.success(list);

    }

    @GetMapping("/fire/equipment")
    public R getEquipmentList(String device_id, String start, String end,
                             @RequestParam(name = "page", defaultValue = "1") Integer page,
                             @RequestParam(name = "size", defaultValue = "15") Integer size)
    {
        HashMap where = new HashMap();
        where.put("device_id",device_id);
        where.put("start", start);
        where.put("end", end);

        Page<HashMap> pages = new Page<>(page, size);
        Page<HashMap> list = equipmentService.getList(pages, where);


        return R.success(list);

    }



}
