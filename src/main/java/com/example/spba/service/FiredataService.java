package com.example.spba.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.spba.domain.entity.Firedata;

import java.util.HashMap;

public interface FiredataService extends IService<Firedata> {

    public Page<HashMap> getList(Page page, HashMap params);

}
