package com.example.spba.service;

import com.example.spba.domain.entity.SensorData;

import java.util.List;

public interface SensorDataService {
    List<SensorData> getAllSensorData();
}
