package com.example.spba.service;

import com.google.code.kaptcha.impl.DefaultKaptcha;

import javax.servlet.http.HttpServletRequest;

public interface CaptchaService {
    String generateCaptcha();

    boolean validateCaptcha(String userInputCaptcha);
}
