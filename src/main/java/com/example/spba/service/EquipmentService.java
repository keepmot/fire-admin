package com.example.spba.service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.spba.domain.entity.Equipment;
import java.util.HashMap;

public interface EquipmentService extends IService<Equipment> {

    public Page<HashMap> getList(Page page, HashMap params);



}
