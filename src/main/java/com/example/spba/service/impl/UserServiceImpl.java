package com.example.spba.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.spba.dao.UserMapper;
import com.example.spba.domain.dto.UserPasswordDto;
import com.example.spba.domain.entity.User;
import com.example.spba.exception.ServiceException;
import com.example.spba.service.UserService;


import org.springframework.stereotype.Component;


/**
 * Created by macro on 2023/11/24
 */
@Component
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{



    private  UserMapper userMapper;

    @Override
    public void updatePassword(UserPasswordDto userPasswordDto) throws ServiceException {
        int update = userMapper.updatePassword(userPasswordDto);
        if(update<1){
            throw new ServiceException("密码错误");
        }

    }


    @Override
    public User getUserById(int userId) {
        // 这里是你获取用户信息的具体实现，可以从数据库中查询用户信息等
        // 假设这里返回一个空的用户对象
        return new User(1L,"admin","123456y"); // 这里需要根据实际情况返回具体的用户对象
    }


}