package com.example.spba.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.spba.dao.EquipmentMapper;
import com.example.spba.domain.entity.Equipment;
import com.example.spba.service.EquipmentService;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by macro on 2024/2/25
 */
@Service
public class EquipmentServiceImpl extends ServiceImpl<EquipmentMapper, Equipment> implements EquipmentService
{

    @Override
    public Page<HashMap> getList(Page page, HashMap params) {
        return this.baseMapper.getList(page, params);
    }
}
