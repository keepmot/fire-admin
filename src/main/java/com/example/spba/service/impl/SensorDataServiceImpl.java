package com.example.spba.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.spba.dao.FireLogMapper;
import com.example.spba.dao.RoleMapper;
import com.example.spba.dao.SensorDataMapper;
import com.example.spba.domain.entity.FireLog;
import com.example.spba.domain.entity.Role;
import com.example.spba.domain.entity.SensorData;
import com.example.spba.service.FireLogService;
import com.example.spba.service.SensorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by macro on 2024/3/17
 */
@Service
public class SensorDataServiceImpl extends ServiceImpl<SensorDataMapper, SensorData> implements SensorDataService {
    @Autowired
    private SensorDataMapper sensorDataMapper;

    @Override
    public List<SensorData> getAllSensorData() {
        return sensorDataMapper.selectList(null);
    }
}
