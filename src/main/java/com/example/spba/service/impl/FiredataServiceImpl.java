package com.example.spba.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.spba.dao.FiredataMapper;
import com.example.spba.domain.entity.Firedata;
import com.example.spba.service.FiredataService;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by macro on 2023/12/25
 */
@Service
public class FiredataServiceImpl extends ServiceImpl<FiredataMapper, Firedata> implements FiredataService
{
    @Override
    public Page<HashMap> getList(Page page, HashMap params) {
        return this.baseMapper.getList(page, params);
    }
}