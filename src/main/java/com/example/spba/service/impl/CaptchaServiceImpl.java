package com.example.spba.service.impl;

import com.example.spba.service.CaptchaService;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import java.util.Random;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Override
    public String generateCaptcha() {
        // 生成随机验证码
        Random random = new Random();
        StringBuilder captcha = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            captcha.append(random.nextInt(10));
        }
        return captcha.toString();
    }

    @Override
    public boolean validateCaptcha(String userInputCaptcha) {
        return false;
    }


}
