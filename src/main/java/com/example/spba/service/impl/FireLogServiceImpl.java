package com.example.spba.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.spba.dao.FireLogMapper;
import com.example.spba.domain.entity.FireLog;
import com.example.spba.service.FireLogService;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by macro on 2023/12/21
 */
@Service
public class FireLogServiceImpl extends ServiceImpl<FireLogMapper, FireLog> implements FireLogService
{

    @Override
    public Page<HashMap> getList(Page page, HashMap params) {
        return this.baseMapper.getList(page, params);
    }
}