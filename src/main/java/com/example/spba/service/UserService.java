package com.example.spba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.spba.domain.dto.UserPasswordDto;
import com.example.spba.domain.entity.User;
import com.example.spba.exception.ServiceException;

public interface UserService extends IService<User> {

    void updatePassword(UserPasswordDto userPasswordDto) throws ServiceException;

    User getUserById(int userId);
}
